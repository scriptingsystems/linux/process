```ssh
# cat PROJECT/script1.sh
echo "Sleeping for 60 seconds...."
sleep 60
echo "Slept for 60 seconds"
```

```ssh
# bash PROJECT/script1.sh
Sleeping for 60 seconds....
```

```ssh
# pstree -p 2350
bash(2350)───sleep(2351)
```

```ssh
# [[ -d /proc/2350 ]] && echo "OK"
OK
```

```ssh
# bash PROJECT/script1.sh
Sleeping for 60 seconds....
Slept for 60 seconds
```

```ssh
# pstree -p 2350
# [[ -d /proc/2350 ]] && echo "OK"
```